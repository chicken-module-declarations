chicken-module-declarations
===========================
Extends CHICKEN's [declaration][] mechanism with forms for controlling
modules, imports and exports.

[declaration]: https://api.call-cc.org/doc/chicken/declarations

API
---

    (declare (module alphabet-soup) ; Wraps the current file in a module
             (import abc def ghi)   ; that uses these three libraries
             (export uvw xyz))      ; and exports these two identifiers.

Requirements
------------
  * [matchable](https://wiki.call-cc.org/egg/matchable)

Installation
------------
    $ git clone http://git.foldling.org/chicken-module-declarations.git
    $ cd chicken-module-declarations
    $ chicken-install

Usage
-----
Load `module-declarations` as a compiler extension with the `-extend`
flag to `csc`(1):

    $ csc -extend module-declarations <file>

Documentation is available at [chickadee][] and on the [CHICKEN
wiki][wiki].

[chickadee]: https://api.call-cc.org/doc/module-declarations
[wiki]: https://wiki.call-cc.org/egg/module-declarations

Author
------
Evan Hanson <evhan@foldling.org>

License
-------
BSD. See LICENSE for details.
